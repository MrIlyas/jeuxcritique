﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1;

namespace WebApplication1.Controllers
{
    [Authorize]
    public class criticsController : Controller
    {
        private JCModelEntities db = new JCModelEntities();

        // GET: critics
        [AllowAnonymous]
        public ActionResult Index()
        {
            var critics = db.critics.Include(c => c.games);
            return View(critics.ToList());
        }

        // GET: critics/Details/5
        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            critics critics = db.critics.Find(id);
            //comments comments = db.comments.Where(g => g.id_critic == id);
            if (critics == null)
            {
                return HttpNotFound();
            }
            return View(critics);
        }

        // GET: critics/Create
        public ActionResult Create()
        {
            ViewBag.category_id = new SelectList(db.category, "id_category", "category_name");
            ViewBag.game_id = new SelectList(db.games, "game_id", "name");
            ViewBag.user_id = new SelectList(db.user, "user_id", "username");
            return View();
        }

        // POST: critics/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HttpPostedFileBase critic_header, [Bind(Include = "critic_id,critic_header,critic_content,title,subtitle,critic_date_published,critic_date_reviewed,is_published,category_id,game_id,user_id")] critics critics)
        {
            String ct = "";
            //verify that the file is selected and not empty
            if (critic_header != null && critic_header.ContentLength > 0)
            {
                //getting the name of the file
                var fileName = Path.GetFileName(critic_header.FileName);

                //store file in the Books folder
                var path = Path.Combine(Server.MapPath("~/Images"), fileName);
                try
                {
                    critic_header.SaveAs(path);
                    ct = critic_header.FileName;
                }
                catch (Exception ex)
                {

                }
            }

            if (ModelState.IsValid)
            {
                critics.critic_header = ct;
                db.critics.Add(critics);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.game_id = new SelectList(db.games, "game_id", "name", critics.game_id);
            return View(critics);
        }

        // GET: critics/Edit/5

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            critics critics = db.critics.Find(id);
            if (critics == null)
            {
                return HttpNotFound();
            }
            ViewBag.game_id = new SelectList(db.games, "game_id", "name", critics.game_id);
            return View(critics);
        }

        // POST: critics/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(HttpPostedFileBase critic_header, [Bind(Include = "critic_id,critic_header,critic_content,title,subtitle,critic_date_published,critic_date_reviewed,is_published,category_id,game_id,user_id")] critics critics)
        {
            String ct = "";
            //verify that the file is selected and not empty
            if (critic_header != null && critic_header.ContentLength > 0)
            {
                //getting the name of the file
                var fileName = Path.GetFileName(critic_header.FileName);

                //store file in the Books folder
                var path = Path.Combine(Server.MapPath("~/Images"), fileName);
                try
                {
                    critic_header.SaveAs(path);
                    ct = critic_header.FileName;
                }
                catch (Exception ex)
                {

                }
            }
            if (ModelState.IsValid)
            {
                if (critics.critic_header != ct) {
                    critics.critic_header = ct;
                }
                db.Entry(critics).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.game_id = new SelectList(db.games, "game_id", "name", critics.game_id);
            return View(critics);
        }

        // GET: critics/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            critics critics = db.critics.Find(id);
            if (critics == null)
            {
                return HttpNotFound();
            }
            return View(critics);
        }

        // POST: critics/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            critics critics = db.critics.Find(id);
            db.critics.Remove(critics);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        [AllowAnonymous]
        public ActionResult ShowCritics() {
            
            return View(db.critics.ToList());
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
