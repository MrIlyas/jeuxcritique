﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
   
    [Authorize]
    public class AdminController : Controller
    {
        private JCModelEntities db = new JCModelEntities();
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Games()
        {
            return View(db.games.ToList());
        }
    }
}
