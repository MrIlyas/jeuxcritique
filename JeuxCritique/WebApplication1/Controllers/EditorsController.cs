﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1;

namespace WebApplication1.Controllers
{
    [Authorize]
    public class EditorsController : Controller
    {
        private JCModelEntities db = new JCModelEntities();

        // GET: Editors
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View(db.editors.ToList());
        }

        // GET: Editors/Details/5
        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            editors editors = db.editors.Find(id);
            if (editors == null)
            {
                return HttpNotFound();
            }
            return View(editors);
        }

        // GET: Editors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Editors/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "editor_id,name,website")] editors editors)
        {
            if (ModelState.IsValid)
            {
                db.editors.Add(editors);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(editors);
        }

        // GET: Editors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            editors editors = db.editors.Find(id);
            if (editors == null)
            {
                return HttpNotFound();
            }
            return View(editors);
        }

        // POST: Editors/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "editor_id,name,website")] editors editors)
        {
            if (ModelState.IsValid)
            {
                db.Entry(editors).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(editors);
        }

        // GET: Editors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            editors editors = db.editors.Find(id);
            if (editors == null)
            {
                return HttpNotFound();
            }
            return View(editors);
        }

        // POST: Editors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            editors editors = db.editors.Find(id);
            db.editors.Remove(editors);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
