﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1;

namespace WebApplication1.Controllers
{
    [Authorize]
    public class GamesController : Controller
    {
        private JCModelEntities db = new JCModelEntities();

        #region (methode de récupération de la dropdownlist pour la classification PEGI)

        private List<SelectListItem> norme_pegi() {
            List <SelectListItem> listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem
            {
                Text = "Pegi 3",
                Value = "3"
            });
            listItems.Add(new SelectListItem
            {
                Text = "Pegi 7",
                Value = "7",
            });
            listItems.Add(new SelectListItem
            {
                Text = "Pegi 12",
                Value = "12"
            });
            listItems.Add(new SelectListItem
            {
                Text = "Pegi 16",
                Value = "16"
            });
            listItems.Add(new SelectListItem
            {
                Text = "Pegi 18",
                Value = "18"
            });
            return listItems;
        }
        #endregion

        // GET: Games
        [AllowAnonymous]
        public ActionResult Index()
        {
            var games = db.games.Include(g => g.developpers).Include(g => g.editors).Include(g=> g.games_consoles);
            return View(games.ToList());
        }

        // GET: Games/Details/5
        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            games games = db.games.Find(id);
            if (games == null)
            {
                return HttpNotFound();
            }
            return View(games);
        }

        // GET: Games/Create
        public ActionResult Create()
        {
            ViewBag.developper_id = new SelectList(db.developpers, "developper_id", "name");
            ViewBag.editor_id = new SelectList(db.editors, "editor_id", "name");
            ViewBag.Consoles = new SelectList(db.consoles, "console_id", "console_name");
            ViewBag.Pegi = norme_pegi();
            return View();
        }

        // POST: Games/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost,ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HttpPostedFileBase jacket, [Bind(Include = "game_id,name,jacket,pegi,description,gender_id,consoles_id,editor_id,developper_id")] games games)
        {
            String ct = "";
            //verify that the file is selected and not empty
            if (jacket != null && jacket.ContentLength > 0)
            {
                //getting the name of the file
                var fileName = Path.GetFileName(jacket.FileName);

                //store file in the Books folder
                var path = Path.Combine(Server.MapPath("~/Images"), fileName);
                try
                {
                    jacket.SaveAs(path);
                    ct = jacket.FileName;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            if (ModelState.IsValid)
            {
                games.jacket = ct;
                db.games.Add(games);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.developper_id = new SelectList(db.developpers, "developper_id", "name", games.developper_id);
            ViewBag.editor_id = new SelectList(db.editors, "editor_id", "name", games.editor_id);
            return View(games);
        }

        // GET: Games/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.Pegi = norme_pegi();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            games games = db.games.Find(id);
            if (games == null)
            {
                return HttpNotFound();
            }
            ViewBag.developper_id = new SelectList(db.developpers, "developper_id", "name", games.developper_id);
            ViewBag.editor_id = new SelectList(db.editors, "editor_id", "name", games.editor_id);
            return View(games);
        }

        // POST: Games/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost,ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(HttpPostedFileBase jacket, [Bind(Include = "game_id,name,jacket,pegi,description,gender_id,consoles_id,editor_id,developper_id")] games games)
        {
            String ct = "";
            //verify that the file is selected and not empty
            if (jacket != null && jacket.ContentLength > 0)
            {
                //getting the name of the file
                var fileName = Path.GetFileName(jacket.FileName);

                //store file in the Books folder
                var path = Path.Combine(Server.MapPath("~/Images"), fileName);
                try
                {
                    jacket.SaveAs(path);
                    ct = jacket.FileName;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            if (ModelState.IsValid)
            {
                if (games.jacket != ct && ct != "")
                {
                    games.jacket = ct;
                }
                db.Entry(games).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.developper_id = new SelectList(db.developpers, "developper_id", "name", games.developper_id);
            ViewBag.editor_id = new SelectList(db.editors, "editor_id", "name", games.editor_id);
            return View(games);
        }
        [Authorize]
        public ActionResult Critics(int? id)
        {
            if (id == null) {
                return RedirectToAction("Index");
            }
            ViewBag.game = db.games;
            return View();
        }

        // POST: critics/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Critics([Bind(Include = "critic_id,critic_content,critic_header,title,subtitle,critic_date_published,critic_date_reviewed,is_published,category_id,game_id,user_id")] critics critics)
        {
  

            if (ModelState.IsValid)
            {
                db.critics.Add(critics);
                db.SaveChanges();
                ViewBag.Message = "Critique crée, merci !";
                return RedirectToAction("Index");
            }

            ViewBag.game_id = new SelectList(db.games, "game_id", "name", critics.game_id);
            return View(critics);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
