﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class GKModel
    {
        public String titre { get; set; }
        public String auteur { get; set; }
        public String lien { get; set; }
        public DateTime datePublication { get; set; }
        public String description { get; set; }
        public String img { get; set; }
    }
}
