﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1;

namespace WebApplication1.Controllers
{
    [Authorize]
    public class ConsolesController : Controller
    {
        private JCModelEntities db = new JCModelEntities();

        // GET: Consoles
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View(db.consoles.ToList());
        }

        // GET: Consoles/Details/5
        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            consoles consoles = db.consoles.Find(id);
            if (consoles == null)
            {
                return HttpNotFound();
            }
            return View(consoles);
        }

        // GET: Consoles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Consoles/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "console_id,console_name,console_displayname,console_birthday_eu,console_birthday_us,console_birthday_jap,constructor_name")] consoles consoles)
        {
            if (ModelState.IsValid)
            {
                db.consoles.Add(consoles);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(consoles);
        }

        // GET: Consoles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            consoles consoles = db.consoles.Find(id);
            if (consoles == null)
            {
                return HttpNotFound();
            }
            return View(consoles);
        }

        // POST: Consoles/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "console_id,console_name,console_displayname,console_birthday_eu,console_birthday_us,console_birthday_jap,constructor_name")] consoles consoles)
        {
            if (ModelState.IsValid)
            {
                db.Entry(consoles).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(consoles);
        }

        // GET: Consoles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            consoles consoles = db.consoles.Find(id);
            if (consoles == null)
            {
                return HttpNotFound();
            }
            return View(consoles);
        }

        // POST: Consoles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            consoles consoles = db.consoles.Find(id);
            db.consoles.Remove(consoles);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
