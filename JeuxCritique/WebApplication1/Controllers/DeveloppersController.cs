﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1;

namespace WebApplication1.Controllers
{
    [Authorize]
    public class DeveloppersController : Controller
    {
        private JCModelEntities db = new JCModelEntities();

        // GET: Developpers
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View(db.developpers.ToList());
        }

        // GET: Developpers/Details/5
        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            developpers developpers = db.developpers.Find(id);
            if (developpers == null)
            {
                return HttpNotFound();
            }
            return View(developpers);
        }

        // GET: Developpers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Developpers/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "developper_id,name,website")] developpers developpers)
        {
            if (ModelState.IsValid)
            {
                db.developpers.Add(developpers);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(developpers);
        }

        // GET: Developpers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            developpers developpers = db.developpers.Find(id);
            if (developpers == null)
            {
                return HttpNotFound();
            }
            return View(developpers);
        }

        // POST: Developpers/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "developper_id,name,website")] developpers developpers)
        {
            if (ModelState.IsValid)
            {
                db.Entry(developpers).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(developpers);
        }

        // GET: Developpers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            developpers developpers = db.developpers.Find(id);
            if (developpers == null)
            {
                return HttpNotFound();
            }
            return View(developpers);
        }

        // POST: Developpers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            developpers developpers = db.developpers.Find(id);
            db.developpers.Remove(developpers);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
