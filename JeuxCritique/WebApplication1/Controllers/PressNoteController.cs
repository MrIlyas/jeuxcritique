﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class PressNoteController : Controller
    {
        // GET: PressNote
        public ActionResult Index()
        { 
            string gamekult = "http://www.gamekult.com/feeds/test.html";
            string gameblog = "http://www.gameblog.fr/rss.php?type=tests";
            ViewBag.Gamekult = RSSCrawler(gamekult, 10);
            ViewBag.Gameblog = RSSCrawler(gameblog, 10);

            return View();
        }
        #region RSSCrawler (methode)
        public List<GKModel> RSSCrawler(String urlrss, int range) {
            List<GKModel> rss = new List<GKModel>();
            try
            {
                XDocument xDoc = new XDocument();
                xDoc = XDocument.Load(urlrss);
                var items = (from x in xDoc.Descendants("item")
                             select new
                             {
                                 title = x.Element("title").Value,
                                 auteur = x.Element("author").Value,
                                 description = x.Element("description").Value,
                                 lien = x.Element("link").Value,
                                 datePublication = x.Element("pubDate"),
                                 image = x.Element("enclosure").Attribute("url").Value
                             }).Take(range);
                System.Diagnostics.Debug.WriteLine(items);
                if (items != null)
                {
                    foreach (var i in items)
                    {
                        GKModel gk = new GKModel
                        {
                            titre = i.title,
                            auteur = i.auteur,
                            description = i.description,
                            lien = i.lien,
                            datePublication = (DateTime)i.datePublication,
                            img = i.image,
                        };
                        rss.Add(gk);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return rss;
        }
        #endregion
    }
}
