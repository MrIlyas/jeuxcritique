﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1;

namespace WebApplication1.Controllers
{
    public class Games_ConsolesController : Controller
    {
        private JCModelEntities db = new JCModelEntities();

        // GET: Games_Consoles
        public ActionResult Index()
        {
            var games_consoles = db.games_consoles.Include(g => g.consoles).Include(g => g.games);
            return View(games_consoles.ToList());
        }

        // GET: Games_Consoles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            games_consoles games_consoles = db.games_consoles.Find(id);
            if (games_consoles == null)
            {
                return HttpNotFound();
            }
            return View(games_consoles);
        }

        // GET: Games_Consoles/Create
        public ActionResult Create()
        {
            ViewBag.id_console = new SelectList(db.consoles, "console_id", "console_name");
            ViewBag.id_game = new SelectList(db.games, "game_id", "name");
            return View();
        }

        // POST: Games_Consoles/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_game,id_console,jaquette,id_games_consoles")] games_consoles games_consoles)
        {
            if (ModelState.IsValid)
            {
                db.games_consoles.Add(games_consoles);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_console = new SelectList(db.consoles, "console_id", "console_name", games_consoles.id_console);
            ViewBag.id_game = new SelectList(db.games, "game_id", "name", games_consoles.id_game);
            return View(games_consoles);
        }

        // GET: Games_Consoles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            games_consoles games_consoles = db.games_consoles.Find(id);
            if (games_consoles == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_console = new SelectList(db.consoles, "console_id", "console_name", games_consoles.id_console);
            ViewBag.id_game = new SelectList(db.games, "game_id", "name", games_consoles.id_game);
            return View(games_consoles);
        }

        // POST: Games_Consoles/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_game,id_console,jaquette,id_games_consoles")] games_consoles games_consoles)
        {
            if (ModelState.IsValid)
            {
                db.Entry(games_consoles).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_console = new SelectList(db.consoles, "console_id", "console_name", games_consoles.id_console);
            ViewBag.id_game = new SelectList(db.games, "game_id", "name", games_consoles.id_game);
            return View(games_consoles);
        }

        // GET: Games_Consoles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            games_consoles games_consoles = db.games_consoles.Find(id);
            if (games_consoles == null)
            {
                return HttpNotFound();
            }
            return View(games_consoles);
        }

        // POST: Games_Consoles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            games_consoles games_consoles = db.games_consoles.Find(id);
            db.games_consoles.Remove(games_consoles);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
